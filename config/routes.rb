Rails.application.routes.draw do
  root to: 'template#index'
  get '/templates' => 'template#show'
end
