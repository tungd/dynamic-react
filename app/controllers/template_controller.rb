class TemplateController < ApplicationController

  def index
    render 'index'
  end

  def show

    @template = <<-EOF
      <h1>Hello, World!</h1>
    EOF

    render 'show'
  end
end
